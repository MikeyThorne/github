import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  primaryKey: 'number',
  normalize(modelClass, responseHash, prop) {
    responseHash.commentCount = responseHash.comments;
    delete responseHash.comments;

    responseHash.links = {
      comments: responseHash.comments_url
    };

    return this._super(modelClass, responseHash, prop);
  }
});
