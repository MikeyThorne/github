import Ember from 'ember';
import Issue from 'github/models/issue';

export default Ember.Route.extend({
  model(params) {
    let repo = this.modelFor('repo').repo;
    let issue = this.store.findRecord('issue', params.issueId, { preload: {repo: repo.get('id')} });

    return Ember.RSVP.hash({
      issue,
      attributes: Ember.get(Issue, 'attributes')
    });
  }
});
