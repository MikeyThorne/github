import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo, hasMany } from 'ember-data/relationships';

export default Model.extend({
  repo: belongsTo('repo'),

  url: attr(),
  number: attr(),
  title: attr(),
  user: attr(),
  labels: attr(),
  state: attr(),
  locked: attr(),
  assignee: attr(),
  milestone: attr(),
  commentCount: attr(),
  createdAt: attr(),
  updatedAt: attr(),
  closedAt: attr(),
  pullRequest: attr(),
  body: attr(),
  closedBy: attr(),
  githubId: attr(),
  comments: hasMany('comment')
});
